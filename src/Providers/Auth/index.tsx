import { createContext, Dispatch, ReactNode, SetStateAction, useContext, useState } from "react";
import api from "../../Services/api";
import {History} from "history";
import { IUserData } from "../../Interfaces";


interface AuthProviderProps {
    children: ReactNode;
}

interface IAuthProviderData {
    token: string;
    setAuth: Dispatch<SetStateAction<string>>;
    signIn: (userData: IUserData, setError: Dispatch<SetStateAction<boolean>>, history: History) => void;
}


const AuthContext = createContext<IAuthProviderData>({} as IAuthProviderData);

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const token = localStorage.getItem("token") || "";

  const [auth, setAuth] = useState<string>(token);

  const signIn = (userData: IUserData, setError: Dispatch<SetStateAction<boolean>>, history: History) => {
    api
      .post("/sessions/", userData)
      .then((response) => {
        localStorage.setItem("token", response.data.access);
        setAuth(response.data.access);
        history.push("/dashboard");
      })
      .catch((_) => setError(true));
  };

  return (
    <AuthContext.Provider value={{ token: auth, setAuth, signIn }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
