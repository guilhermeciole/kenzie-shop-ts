import { Dispatch, SetStateAction } from "react";
import { ReactNode } from "react";
import { createContext, useContext, useEffect, useState } from "react";
import {IProduct} from "../../Interfaces/index"

interface IcartProviderData {
    cart: IProduct[];
    setCart: Dispatch<SetStateAction<IProduct[]>>;
}

interface ICartProviderProps {
    children: ReactNode;
}

const CartContext = createContext<IcartProviderData>({} as IcartProviderData);

export const CartProvider = ({ children }: ICartProviderProps) => {
  const [cart, setCart] = useState<IProduct[]>(
    JSON.parse(localStorage.getItem("cart")!) || []);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <CartContext.Provider value={{ cart, setCart }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
