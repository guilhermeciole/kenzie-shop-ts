import { ReactNode } from "react"
import { AuthProvider } from "./Auth"
import { CartProvider } from "./Cart"

interface ProvidersProps {
    children: ReactNode;
}

const Providers = ({children}: ProvidersProps) => {
    return (
        <CartProvider>
            <AuthProvider>
                {children}
            </AuthProvider>
        </CartProvider>
    )
};

export default Providers;