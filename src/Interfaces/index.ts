export interface IProduct {
    id: number;
    name: string;
    title: string;
    image_url: string;
    priceFormatted?: number;
    price: number;
    description: string;
}

export interface IUserData {
    username: string;
    password: string;
  }