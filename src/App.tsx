import React from "react";
import Routes from "./routes/index";
import GlobalStyle from "./styles/global";
import Header from "./Components/Header";

function App() {
  return (
    <>
      <Header />
      <Routes />
      <GlobalStyle />
    </>
  );
}

export default App;